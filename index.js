const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const PORT = process.env.PORT || 3000;

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

const appRoutes = require('./routes/appRoutes');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(appRoutes);

app.listen(PORT, function(){
    console.log(`App running on port ${PORT}`);
})