CREATE TABLE applications (
    id SERIAL,
    national_ID TEXT,
    name TEXT,
    phone TEXT,
    grade INT,

    PRIMARY KEY (id)
);