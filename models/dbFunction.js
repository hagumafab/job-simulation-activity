// CONNECT TO DATABASE USING POOLS

const Pool = require('pg').Pool;
const pool = new Pool({
    connectionString: process.env.DATABASE_URL,
    ssl: true,
});

module.exports = {
    getData: async function(nationalId){
        try {
            let sql = 'SELECT name, phone, grade FROM applications WHERE national_ID = $1';
            let values = [nationalId];
            let result = await pool.query(sql, values);

            if (result.rowCount > 0) {
                return result.rows[0];
            } else return [];

        } catch (error) {
            console.log(error);
        }
    }
}


//  ########  SWITCHING BETWEEN DATABASE CONNETCTIONS  #######


/*
// CONNECT TO DATABASE USING CLIENT

const { Client } = require('pg');
const client = new Client({
    connectionString: process.env.DATABASE_URL,
    ssl: true,
});
client.connect();

const getData = (nationalId) =>{
    let sql = 'SELECT name, phone, grade FROM applications WHERE national_ID = \'';
    let fullQuery = sql + nationalId + '\';'; 
    client.query(fullQuery, (err, result) =>{
        if(err) console.log(err);
        let name = result.rows[0].name;
        let phone = result.rows[0].phone;
        let grade = result.rows[0].grade;

        return {name: name, phone: phone, grade: grade};
    })
}

module.exports = getData;
*/