const express = require('express');
const router = express.Router();
const db = require('../models/dbFunction');

const nforce = require('nforce');
const org = require('./connection');

router.get('/', (req, res) =>{
    res.render('index');
})

router.get('/app', (req, res) =>{
    res.render('app');
})

// THE POST FROM SALESFORCE WILL LAND HERE
router.post('/app', async (req, res) =>{
    // get the national id and salesforce id from the post body
    let natId = req.body.idNumber;
    let sfId = req.body.sfId;
    console.log(`National_ID: ${natId}  &&  SF_ID: ${sfId}`);

    // get the name phone and grade from the database
    let result = await db.getData(natId);
    console.log(`Name: ${result.name}  &&  Phone: ${result.phone}  &&  Grade: ${result.grade}`);
    
    // create a salesforce object of type Application__c with the id retrived from body 
    let applic = nforce.createSObject('Application__c');
    applic.set('Id', sfId);
    applic.set('Name__c', result.name);
    applic.set('Phone_number__c', result.phone);
    applic.set('Grade__c', result.grade);

    // do update 
    org.update({sobject: applic}).then(function(){
        res.redirect('/');
    })
});

module.exports = router;