// FOR PRODUCTION
const nforce = require('nforce');
const config = require('./config');

const theRedirectUri = 'https://job-simulation-heroku.herokuapp.com';
const theClientId = '3MVG9L8AofS.N0y7O9AMWwru.v45mtcOz1S4GgLLr_6vCMggFpZai1giNexbGMTo0Dyc7frgwojj6XdttS5RE';
const theClientSecret = config.clientSecret;

const username = config.username;
const password = config.password;

const org = nforce.createConnection({
    clientId : theClientId,
    clientSecret: theClientSecret,
    redirectUri: theRedirectUri,
    environment: 'production',
    mode: 'single'
});

org.authenticate({username, password}, function(err, resp){
    // the oauth object was stored in the connection object
    if(!err) console.log('Successfully connected to Salesforce. Cached token: ' + org.oauth.access_token);
    if(err) console.log('Cannot connect to Salesforce: ' + err);
});
  
module.exports = org;